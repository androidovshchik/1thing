package rf.androidovshchik.onething.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yandex.metrica.YandexMetrica;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.LocalDate;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnFocusChange;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Event;
import rf.androidovshchik.onething.data.dao.DayAim;
import rf.androidovshchik.onething.data.dbhelpers.DayAimDbHelper;
import rf.androidovshchik.onething.utils.ViewUtil;
import timber.log.Timber;

public class TodayFragment extends BaseFragment {

    @BindView(R.id.aim_edit)
    EditText aimEdit;

    @BindView(R.id.today_container)
    LinearLayout todayContainer;
    @BindView(R.id.today_aim)
    TextView todayAim;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.is_done)
    TextView isDone;

    private DayAim dayAim = new DayAim();

    private DayAimDbHelper mDayAimDbHelper;
    public TodayFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_today, container, false);
        unbinder = ButterKnife.bind(this, view);
        mDayAimDbHelper = new DayAimDbHelper(mDatabase);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void onEditOrDone(boolean edit) {
        if (edit) {
            todayContainer.setVisibility(View.GONE);
            aimEdit.setVisibility(View.VISIBLE);
            aimEdit.requestFocus();
            aimEdit.setSelection(aimEdit.getText().length());
            ViewUtil.showKeyboard(getApplicationContext());
        } else {
            aimEdit.setVisibility(View.GONE);
            todayContainer.setVisibility(View.VISIBLE);
            if (getMainActivity().isKeyboardShown) {
                ViewUtil.hideKeyboard(getApplicationContext());
            }
        }
    }

    @Override
    public void refresh() {
        setDayAim();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
    public void setDayAim() {
        DayAim dayAim = mDayAimDbHelper.selectOne(LocalDate.now());

        if (dayAim != null) {
            todayAim.setText(dayAim.getAim());
            aimEdit.setText(dayAim.getAim());
            if (dayAim.isDone()) {
//                getMainActivity().edit.setVisible(false);
//                getMainActivity().done.setVisible(false);
                image.setBackground(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.accent_circle_solid_background));
                isDone.setText(R.string.today_done);
            } else {
 //               getMainActivity().edit.setVisible(getMainActivity().showEditToday);
  //              getMainActivity().done.setVisible(!getMainActivity().showEditToday);
                image.setBackground(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.accent_circle_background));
                isDone.setText(R.string.today_undone);
            }
        } else {
            todayAim.setText(R.string.push_appointment);
             aimEdit.setText(R.string.push_appointment);
        }

    }

    @OnClick(R.id.image)
    void onImage() {
        if (dayAim.isDone()) {
            return;
        }
        getMainActivity().edit.setVisible(false);
        getMainActivity().done.setVisible(false);
        image.setBackground(ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.accent_circle_solid_background));
        isDone.setText(R.string.today_done);
        saveAimDone();
    }

    @Override
    public void onStop() {
        super.onStop();
        Timber.d("onStop");
    }

    @OnFocusChange(R.id.aim_edit)
    public void saveAim(boolean focused) {
        if (!focused) {
            dayAim.setAim(aimEdit.getText().toString());
            dayAim.setDone(false);
            dayAim.setDate(LocalDate.now());
            todayAim.setText(dayAim.getAim());

            YandexMetrica.reportEvent(Event.MODIFY_DAY_AIM.toString());
            mFirebaseAnalytics.logEvent(Event.MODIFY_DAY_AIM.toString(), null);
            mDayAimDbHelper.updateOrInsert(dayAim);

        }
    }

    public void saveAimDone() {
        dayAim.setAim(aimEdit.getText().toString());
        dayAim.setDone(true);
        dayAim.setDate(LocalDate.now());
        YandexMetrica.reportEvent(Event.FINISH_DAY_AIM.toString());
        mFirebaseAnalytics.logEvent(Event.FINISH_DAY_AIM.toString(), null);
        mDayAimDbHelper.updateOrInsert(dayAim);
    }
}
