package rf.androidovshchik.onething.ui.main;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class TabsFragmentAdapter extends FragmentStatePagerAdapter {

    private final ArrayList<BaseFragment> fragments;

    public TabsFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        fragments = new ArrayList<>();
        fragments.add(new YearFragment());
        fragments.add(new TodayFragment());
        fragments.add(new CalendarFragment());
        fragments.add(new SettingsFragment());
    }

    @Override
    public BaseFragment getItem(int position) {
        BaseFragment fragment = fragments.get(position);

        fragment.setRefresh(true);

        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}