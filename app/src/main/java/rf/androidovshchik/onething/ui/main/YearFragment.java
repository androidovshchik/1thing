package rf.androidovshchik.onething.ui.main;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.yandex.metrica.YandexMetrica;

import org.joda.time.LocalDate;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Event;
import rf.androidovshchik.onething.data.dao.DayAim;
import rf.androidovshchik.onething.data.dao.YearAims;
import rf.androidovshchik.onething.data.dbhelpers.DayAimDbHelper;
import rf.androidovshchik.onething.data.dbhelpers.YearAimsDbHelper;
import rf.androidovshchik.onething.utils.ViewUtil;

public class YearFragment extends BaseFragment {

    @BindView(R.id.aim1_container)
    LinearLayout aim1Container;
    @BindView(R.id.aim1_text)
    TextView aim1Text;
    @BindView(R.id.aim1_box)
    AppCompatCheckBox aim1Box;
    @BindView(R.id.aim2_container)
    LinearLayout aim2Container;
    @BindView(R.id.aim2_text)
    TextView aim2Text;
    @BindView(R.id.aim2_box)
    AppCompatCheckBox aim2Box;
    @BindView(R.id.aim3_container)
    LinearLayout aim3Container;
    @BindView(R.id.aim3_text)
    TextView aim3Text;
    @BindView(R.id.aim3_box)
    AppCompatCheckBox aim3Box;

    @BindView(R.id.aim1_edit)
    EditText aim1Edit;
    @BindView(R.id.aim2_edit)
    EditText aim2Edit;
    @BindView(R.id.aim3_edit)
    EditText aim3Edit;

    @BindView(R.id.footer)
    LinearLayout footer;
    @BindView(R.id.movement)
    TextView movement;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.done)
    TextView done;
    @BindView(R.id.undone)
    TextView undone;

    private YearAimsDbHelper mYearAimsDbHelper;
    private DayAimDbHelper mDayAimDbHelper;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_year, container, false);
        unbinder = ButterKnife.bind(this, view);

        mYearAimsDbHelper = new YearAimsDbHelper(mDatabase);
        mDayAimDbHelper = new DayAimDbHelper(mDatabase);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void refresh() {
        onEditOrDone(false);
        selectOneYear();
        setDoneAndUndoneTasks();
    }

    @Override
    public void onEditOrDone(boolean edit) {
        changeMode(edit);

        if (edit) {
            aim1Edit.requestFocus();
            ViewUtil.showKeyboard(getApplicationContext());
        } else {
            if (getMainActivity().isKeyboardShown) {
                YandexMetrica.reportEvent(Event.MODIFY_YEAR_AIM.toString());
                mFirebaseAnalytics.logEvent(Event.MODIFY_YEAR_AIM.toString(), null);

                updateAimsInYear();
                selectOneYear();
                ViewUtil.hideKeyboard(getApplicationContext());
            }
        }
    }

    public void selectOneYear() {
        YearAims yearAims = mYearAimsDbHelper.selectOne(LocalDate.now().getYear());

        if (yearAims == null) {
            yearAims = YearAimsDbHelper.getYearArmsSample(getApplicationContext());
        }

        aim1Text.setText(yearAims.getAim1());
        aim2Text.setText(yearAims.getAim2());
        aim3Text.setText(yearAims.getAim3());
        aim1Edit.setText(yearAims.getAim1());
        aim2Edit.setText(yearAims.getAim2());
        aim3Edit.setText(yearAims.getAim3());
        aim1Box.setChecked(yearAims.isDone1());
        aim2Box.setChecked(yearAims.isDone2());
        aim3Box.setChecked(yearAims.isDone3());
    }

    public void updateAimsInYear() {
        YearAims yearAims = new YearAims();
        yearAims.setAim1(aim1Edit.getText().toString());
        yearAims.setAim2(aim2Edit.getText().toString());
        yearAims.setAim3(aim3Edit.getText().toString());
        yearAims.setDone1(aim1Box.isChecked());
        yearAims.setDone2(aim2Box.isChecked());
        yearAims.setDone3(aim3Box.isChecked());
        yearAims.setYear(LocalDate.now().getYear());

        mYearAimsDbHelper.updateOrInsert(yearAims);
    }

    @OnCheckedChanged({R.id.aim1_box, R.id.aim2_box, R.id.aim3_box})
    public void onBoxesCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.aim1_box:
                    aim1Text.setTextColor(CROSSED_COLOR);
                    aim1Text.setPaintFlags(aim1Text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    break;
                case R.id.aim2_box:
                    aim2Text.setTextColor(CROSSED_COLOR);
                    aim2Text.setPaintFlags(aim2Text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    break;
                case R.id.aim3_box:
                    aim3Text.setTextColor(CROSSED_COLOR);
                    aim3Text.setPaintFlags(aim3Text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    break;
            }
        } else {
            switch (button.getId()) {
                case R.id.aim1_box:
                    aim1Text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary));
                    aim1Text.setPaintFlags(aim1Text.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    break;
                case R.id.aim2_box:
                    aim2Text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary));
                    aim2Text.setPaintFlags(aim2Text.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    break;
                case R.id.aim3_box:
                    aim3Text.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary));
                    aim3Text.setPaintFlags(aim3Text.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    break;
            }
        }
    }

    @OnClick({R.id.aim1_box, R.id.aim2_box, R.id.aim3_box})
    public void onTextChanged() {
        YandexMetrica.reportEvent(Event.FINISH_YEAR_AIM.toString());
        mFirebaseAnalytics.logEvent(Event.FINISH_YEAR_AIM.toString(), null);
        updateAimsInYear();
    }

    public void changeMode(boolean edit) {
        aim1Container.setVisibility(edit ? View.GONE : View.VISIBLE);
        aim2Container.setVisibility(edit ? View.GONE : View.VISIBLE);
        aim3Container.setVisibility(edit ? View.GONE : View.VISIBLE);
        footer.setVisibility(edit ? View.GONE : View.VISIBLE);
        aim1Edit.setVisibility(edit ? View.VISIBLE : View.GONE);
        aim2Edit.setVisibility(edit ? View.VISIBLE : View.GONE);
        aim3Edit.setVisibility(edit ? View.VISIBLE : View.GONE);
    }

    public void setDoneAndUndoneTasks() {
        int doneDays = 0;
        int undoneDays = 0;

        List<DayAim> elements = mDayAimDbHelper.selectInYear(LocalDate.now().getYear());

        for (DayAim one : elements) {
            if (one.isDone()) {
                doneDays++;
            } else {
                undoneDays++;
            }
        }

        int allDays = doneDays + undoneDays;

        movement.setText(getString(R.string.year_days_count, allDays + " " + getEnding(allDays)));
        progress.setProgress(Math.round(100f * doneDays / allDays));
        done.setText(getString(R.string.year_done_aims, doneDays + " " + getEnding(doneDays)));
        undone.setText(getString(R.string.year_undone_aims, undoneDays + " " + getEnding(undoneDays)));
    }
}
