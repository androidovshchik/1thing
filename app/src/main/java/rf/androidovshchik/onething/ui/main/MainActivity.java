package rf.androidovshchik.onething.ui.main;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.ui.BaseActivity;
import rf.androidovshchik.onething.ui.views.SelectableImage;

public class MainActivity extends BaseActivity {

    public static final String EXTRA_TAB = "tab";

    @BindView(R.id.pager)
    ViewPager viewPager;
    @BindView(R.id.year)
    SelectableImage year;
    @BindView(R.id.today)
    SelectableImage today;
    @BindView(R.id.calendar)
    SelectableImage calendar;
    @BindView(R.id.settings)
    SelectableImage settings;

    private TabsFragmentAdapter adapter;

    public MenuItem edit;
    public MenuItem done;

    public boolean showEditYear = true;
    public boolean showEditToday = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        adapter = new TabsFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        if (getIntent().getIntExtra(EXTRA_TAB, 0) == 2) {
            onToday();
        } else {
            onYear();
        }
    }

    @OnClick(R.id.year)
    void onYear() {
        setTitle(R.string.title_year);
        setNavigationSelected(R.id.year);
        if (edit != null && done != null) {
            edit.setVisible(showEditYear);
            done.setVisible(!showEditYear);
        }
        viewPager.setCurrentItem(0, false);
    }

    @OnClick(R.id.today)
    void onToday() {
        setTitle(R.string.title_today);
        setNavigationSelected(R.id.today);
        if (edit != null && done != null) {
            edit.setVisible(showEditToday);
            done.setVisible(!showEditToday);
        }
        viewPager.setCurrentItem(1, false);
    }

    @OnClick(R.id.calendar)
    void onCalendar() {
        setTitle(R.string.title_calendar);
        setNavigationSelected(R.id.calendar);
        if (edit != null && done != null) {
            edit.setVisible(false);
            done.setVisible(false);
        }
        viewPager.setCurrentItem(2, false);
    }

    @OnClick(R.id.settings)
    void onSettings() {
        setTitle(R.string.title_settings);
        setNavigationSelected(R.id.settings);
        if (edit != null && done != null) {
            edit.setVisible(false);
            done.setVisible(false);
        }
        viewPager.setCurrentItem(3, false);
    }

    private void setNavigationSelected(@IdRes int id) {
        year.setSelected(id == R.id.year);
        today.setSelected(id == R.id.today);
        calendar.setSelected(id == R.id.calendar);
        settings.setSelected(id == R.id.settings);
    }

    public AlertDialog.Builder buildAlertDialog(@StringRes int title) {
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setNegativeButton(getString(android.R.string.cancel), null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        edit = menu.findItem(R.id.action_edit);
        edit.setVisible(true);
        done = menu.findItem(R.id.action_done);
        done.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                edit.setVisible(false);
                done.setVisible(true);
                if (viewPager.getCurrentItem() == 0) {
                    showEditYear = false;
                    adapter.getItem(0).onEditOrDone(true);
                } else if (viewPager.getCurrentItem() == 1) {
                    showEditToday = false;
                    adapter.getItem(1).onEditOrDone(true);
                }
                return true;
            case R.id.action_done:
                edit.setVisible(true);
                done.setVisible(false);
                if (viewPager.getCurrentItem() == 0) {
                    showEditYear = true;
                    adapter.getItem(0).onEditOrDone(false);
                } else if (viewPager.getCurrentItem() == 1) {
                    showEditToday = true;
                    adapter.getItem(1).onEditOrDone(false);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
