package rf.androidovshchik.onething.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.google.firebase.analytics.FirebaseAnalytics;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.DbHelper;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.models.events.SuggestEvent;
import rf.androidovshchik.onething.utils.SuggestUtil;

public abstract class BaseFragment extends Fragment {

    protected static final int CROSSED_COLOR = Color.parseColor("#60000000");
    public static final String IS_REFRESH = "is_refresh";

    protected Unbinder unbinder;

    protected Prefs prefs;

    protected CompositeDisposable disposable = new CompositeDisposable();

    protected SQLiteDatabase mDatabase;

    private String[] ENDINGS;
    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = DbHelper.getDatabase(getApplicationContext());
        prefs = new Prefs(getApplicationContext());

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    public abstract void onEditOrDone(boolean edit);

    @Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
    public void onSuggestEvent(SuggestEvent event) {
        EventBus.getDefault().removeStickyEvent(SuggestEvent.class);
        if (SuggestUtil.needShow(prefs, getClass())) {
            getMainActivity().buildAlertDialog(R.string.settings_enable_title)
                    .setMessage(R.string.settings_enable_description)
                    .setPositiveButton(getString(R.string.settings_enable), (DialogInterface dialog, int id) -> {
                        SuggestUtil.cancelSuggestions(prefs, -1);
                    })
                    .create()
                    .show();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }

    protected Context getApplicationContext() {
        return getActivity().getApplicationContext();
    }

    public void refresh() {
        // pass
    }


    private void setStrings() {
        if (ENDINGS == null) {
            ENDINGS = new String[] {
                    getString(R.string.day1),
                    getString(R.string.days_end_1),
                    getString(R.string.days_end_234),
                    getString(R.string.days)
            };
        }
    }

    public String getEnding(int count) {
        setStrings();

        if (count == 1) {
            return ENDINGS[0];
        }

        count = count % 100;
        if (count >= 11 && count <= 19) {
            return ENDINGS[3];
        } else {
            switch (count % 10) {
                case 1:
                    return ENDINGS[1];
                case 2: case 3: case 4:
                    return ENDINGS[2];
                default:
                    return ENDINGS[3];
            }
        }
    }

    public void setRefresh(boolean isRefresh) {
        Bundle args = new Bundle();
        args.putBoolean(IS_REFRESH, isRefresh);
        setArguments(args);
    }

    public boolean getRefresh() {
        return getArguments().getBoolean(IS_REFRESH);
    }
}