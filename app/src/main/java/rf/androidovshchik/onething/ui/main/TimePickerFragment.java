package rf.androidovshchik.onething.ui.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;

import rf.androidovshchik.onething.Constants;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.models.events.TimePickerEvent;
import rf.androidovshchik.onething.utils.EventUtil;

public class TimePickerFragment extends DialogFragment {

    private int type = Constants.TYPE_APPOINTMENT;

    private Prefs prefs;

    public TimePickerFragment() {}

    @Nullable
    @Override
    @SuppressWarnings("all")
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SublimePicker sublimePicker = (SublimePicker) getActivity().getLayoutInflater()
                .inflate(R.layout.sublime_picker, container);
        Bundle arguments = getArguments();
        if (arguments != null) {
            type = arguments.getInt(Constants.EXTRA_TYPE);
        }
        prefs = new Prefs(getActivity().getApplicationContext());
        String[] numbers = prefs.getString(type == Constants.TYPE_APPOINTMENT ? Prefs.TIME_APPOINTMENT_PUSH :
                Prefs.TIME_EXECUTION_PUSH).split(":");
        SublimeOptions options = new SublimeOptions();
        options.setDisplayOptions(SublimeOptions.ACTIVATE_TIME_PICKER);
        options.setPickerToShow(SublimeOptions.Picker.TIME_PICKER);
        options.setTimeParams(Integer.parseInt(numbers[0]), Integer.parseInt(numbers[1]), true);
        sublimePicker.initializePicker(options, new SublimeListenerAdapter() {

            @Override
            public void onCancelled() {
                dismiss();
            }

            @Override
            public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker, SelectedDate selectedDate,
                                                int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                                String recurrenceRule) {
                String value = (hourOfDay < 10 ? "0" + hourOfDay : hourOfDay) + ":" +
                        (minute < 10 ? "0" + minute : minute);
                prefs.putString(type == Constants.TYPE_APPOINTMENT ? Prefs.TIME_APPOINTMENT_PUSH :
                        Prefs.TIME_EXECUTION_PUSH, value);
                EventUtil.post(new TimePickerEvent(type, value));
                dismiss();
            }
        });
        return sublimePicker;
    }
}