package rf.androidovshchik.onething.ui;

import android.os.Bundle;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import io.reactivex.Observable;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.ui.main.MainActivity;
import timber.log.Timber;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        if (!prefs.has(Prefs.FIRST_LAUNCH_TIME)) {
            prefs.putLong(Prefs.FIRST_LAUNCH_TIME, System.currentTimeMillis());
         /*   manager.onExecSql("UPDATE days SET start = strftime('%Y-%m-%d', 'now') WHERE rowid = 1")
                    .subscribe((Boolean value) -> manager.onExecSql("UPDATE years SET year = " +
                            Calendar.getInstance().get(Calendar.YEAR) + " WHERE rowid = 1")
                                    .subscribe());*/
        } else if (!prefs.has(Prefs.SUGGESTED_AFTER_3_MINUTES)) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_3_MINUTES, true);
        }
        disposable.add(Observable.just(true).delay(2, TimeUnit.SECONDS)
                .subscribe((Boolean value) -> {
                    Timber.d("Finish waiting splash pause");
                    onSwitchSafelyActivity(MainActivity.class);
                }));
    }
}
