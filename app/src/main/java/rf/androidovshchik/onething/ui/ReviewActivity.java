package rf.androidovshchik.onething.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rf.androidovshchik.onething.R;

public class ReviewActivity extends BaseActivity {

    @BindView(R.id.comment)
    EditText comment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review);
        setTitle(R.string.title_review);
        ButterKnife.bind(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @OnClick(R.id.cancel)
    void onCancel() {
        finish();
    }

    @OnClick(R.id.send)
    void onSend() {
        if (TextUtils.isEmpty(comment.getText())) {
            return;
        }
        String mailTo = "mailto:nechaev.finec@gmail.com" + "?&subject=" + Uri.encode(getString(R.string.review_subject)) +
                "&body=" + Uri.encode(comment.getText().toString());
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(mailTo));
        startActivity(intent);
    }
}
