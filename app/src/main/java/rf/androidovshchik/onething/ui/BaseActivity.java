package rf.androidovshchik.onething.ui;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import butterknife.BindView;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.DbHelper;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.utils.ViewUtil;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.contentView)
    ViewGroup contentView;

    protected CompositeDisposable disposable = new CompositeDisposable();
    protected Prefs prefs;
    protected SQLiteDatabase mDatabase;
    private Rect rectResizedWindow;
    private int windowHeight;
    public boolean isKeyboardShown = false;

    private ViewTreeObserver.OnGlobalLayoutListener observer = () -> {
        getWindow().getDecorView().getWindowVisibleDisplayFrame(rectResizedWindow);
        isKeyboardShown = windowHeight - rectResizedWindow.bottom != 0;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDatabase = DbHelper.getDatabase(getApplicationContext());
        prefs = new Prefs(getApplicationContext());
        rectResizedWindow = new Rect();
        windowHeight = ViewUtil.getWindow(getApplicationContext()).y;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (contentView != null) {
            contentView.requestFocus();
        }
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(observer);
    }

    @Override
    protected void onStop() {
        super.onStop();
        getWindow().getDecorView().getViewTreeObserver().removeOnGlobalLayoutListener(observer);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected boolean onSwitchSafelyActivity(Class activity) {
        if (onStartSafelyActivity(activity)) {
            finish();
            return true;
        }
        return false;
    }

    public boolean onStartSafelyActivity(Class activity) {
        if (!getClass().equals(activity)) {
            Intent intent = new Intent(getApplicationContext(), activity);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
