package rf.androidovshchik.onething.ui.views;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

public class SelectableImage extends AppCompatImageView {

    private static final int COLOR_INACTIVE = Color.parseColor("#6dc0c5");

    public SelectableImage(Context context) {
        super(context);
    }

    public SelectableImage(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectableImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setSelected(boolean selected) {
        if (selected) {
            clearColorFilter();
        } else {
            setColorFilter(COLOR_INACTIVE, PorterDuff.Mode.MULTIPLY);
        }
    }

    @Override
    public boolean hasOverlappingRendering() {
        return false;
    }
}
