package rf.androidovshchik.onething.ui.main;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.yandex.metrica.YandexMetrica;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Event;
import rf.androidovshchik.onething.data.dao.DayAim;
import rf.androidovshchik.onething.data.dbhelpers.DayAimDbHelper;

public class CalendarFragment extends BaseFragment {

    @BindView(R.id.sublime)
    SublimePicker sublimePicker;

    @BindView(R.id.aim_container)
    LinearLayout aimContainer;
    @BindView(R.id.aim_text)
    TextView aimText;
    @BindView(R.id.aim_box)
    AppCompatCheckBox aimBox;
    @BindView(R.id.record)
    TextView record;

    private DayAimDbHelper mDayAimDbHelper;
    private DayAim mDayAim;
    LocalDate currentDate = LocalDate.now();

    public CalendarFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        mDayAimDbHelper = new DayAimDbHelper(mDatabase);

        unbinder = ButterKnife.bind(this, view);
        SublimeOptions options = new SublimeOptions();
        options.setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER);
        sublimePicker.initializePicker(options, new SublimeListenerAdapter() {
            @Override
            public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker,
                                                SelectedDate selectedDate, int hourOfDay, int minute,
                                                SublimeRecurrencePicker.RecurrenceOption recurrenceOption,
                                                String recurrenceRule) {
                currentDate = new LocalDate(selectedDate.getStartDate());
                updateCurrentAim();
            }

            @Override
            public void onCancelled() {
            }
        });

        sublimePicker.mDatePicker.setMaxDate(System.currentTimeMillis());

        List<DayAim> dayAims = mDayAimDbHelper.selectAll();

        Collections.sort(dayAims, new Comparator<DayAim>() {
            @Override
            public int compare(DayAim o1, DayAim o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });

        LocalDate begin = null;
        LocalDate end = null;

        for (DayAim one : dayAims) {
            if (begin == null) {
                begin = one.getDate();
                end = one.getDate();
            } else {
                if (!end.plusDays(1).equals(one.getDate())) {
                    sublimePicker.mDatePicker.mDayPickerView.addRange(begin, end);
                    begin = one.getDate();
                }
                end = one.getDate();
            }
        }

        if (begin != null) {
            sublimePicker.mDatePicker.mDayPickerView.addRange(begin, end);
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }

    @Override
    public void refresh() {
        updateCurrentAim();
        updateRecord();
    }

    private void updateRecord() {
        int days = countSuccessfulDays();
        record.setText(getString(R.string.calendar_record, days + " " + getEnding(days)));
    }

    private int countSuccessfulDays() {
        List<DayAim> aims = mDayAimDbHelper.selectInYear(LocalDate.now().getYear());

        int record = 0;
        int current = 0;
        LocalDate currentDate = LocalDate.now().minusYears(1);


        for (DayAim aim : aims) {
            if (currentDate.plusDays(1).equals(aim.getDate()) && aim.isDone()) {
                current++;
            } else {
                if (current > record) {
                    record = current;
                }
                current = 1;
            }
            currentDate = aim.getDate();
        }

        if (current > record) {
            record = current;
        }

        return record;
    }

    private void updateCurrentAim() {
        mDayAim = mDayAimDbHelper.selectOne(currentDate);
        setAim();
    }

    @Override
    public void onEditOrDone(boolean edit) {}

    @OnCheckedChanged(R.id.aim_box)
    protected void aimBoxChecked(CompoundButton button, boolean checked) {
        if (checked && mDayAim != null) {
            mDayAim.setDone(true);
            setAim();

            DayAim one = mDayAimDbHelper.selectOne(mDayAim.getDate());
            if (one != null && !one.isDone()) {
                YandexMetrica.reportEvent(Event.FINISH_DAY_AIM_AFTER_DAY_FINISHED.toString());
                mFirebaseAnalytics.logEvent(Event.FINISH_DAY_AIM_AFTER_DAY_FINISHED.toString(), null);
            }

            mDayAimDbHelper.updateOrInsert(mDayAim);
        }
    }

    private void setAim() {
        if (mDayAim != null) {
            aimText.setText(mDayAim.getAim());
            aimBox.setChecked(mDayAim.isDone());
            aimBox.setEnabled(!mDayAim.isDone());
            updateUiAim(mDayAim.isDone());
            aimContainer.setVisibility(View.VISIBLE);
        } else {
            aimContainer.setVisibility(View.GONE);
        }
    }

    private void updateUiAim(boolean isDone) {
        if (isDone) {
            aimText.setTextColor(CROSSED_COLOR);
            aimText.setPaintFlags(aimBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            aimText.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.colorTextPrimary));
            aimText.setPaintFlags(aimBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        }
    }
}
