package rf.androidovshchik.onething.ui.main;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import rf.androidovshchik.onething.Constants;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.models.events.SuggestEvent;
import rf.androidovshchik.onething.models.events.TimePickerEvent;
import rf.androidovshchik.onething.ui.ReviewActivity;
import rf.androidovshchik.onething.utils.AlarmUtil;
import rf.androidovshchik.onething.utils.SuggestUtil;

public class SettingsFragment extends BaseFragment {

    @BindView(R.id.time_appointment)
    TextView timeAppointment;
    @BindView(R.id.box_appointment)
    AppCompatCheckBox boxAppointment;
    @BindView(R.id.time_execution)
    TextView timeExecution;
    @BindView(R.id.box_execution)
    AppCompatCheckBox boxExecution;
    @BindView(R.id.star1)
    ImageView star1;
    @BindView(R.id.star2)
    ImageView star2;
    @BindView(R.id.star3)
    ImageView star3;
    @BindView(R.id.star4)
    ImageView star4;
    @BindView(R.id.star5)
    ImageView star5;

    public SettingsFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        ((AppCompatCheckBox) view.findViewById(R.id.box_appointment))
                .setChecked(prefs.getBoolean(Prefs.ENABLE_APPOINTMENT_PUSH));
        ((AppCompatCheckBox) view.findViewById(R.id.box_execution))
                .setChecked(prefs.getBoolean(Prefs.ENABLE_EXECUTION_PUSH));
        unbinder = ButterKnife.bind(this, view);
        timeAppointment.setText(prefs.getString(Prefs.TIME_APPOINTMENT_PUSH));
        timeExecution.setText(prefs.getString(Prefs.TIME_EXECUTION_PUSH));
        setStars(prefs.getInt(Prefs.STARS_COUNT));
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        onSuggestEvent(null);
    }

    @Override
    public void onEditOrDone(boolean edit) {}

    @Override
    @SuppressWarnings("unused")
    @Subscribe(sticky = true, threadMode = ThreadMode.POSTING)
    public void onSuggestEvent(SuggestEvent event) {
        if (SuggestUtil.needShow(prefs, getClass())) {
            getMainActivity().buildAlertDialog(R.string.settings_enable_title)
                    .setMessage(R.string.settings_enable_description)
                    .setPositiveButton(getString(R.string.settings_enable), (DialogInterface dialog, int id) -> {
                        SuggestUtil.cancelSuggestions(prefs, -1);
                        boxAppointment.setChecked(true);
                        boxExecution.setChecked(true);
                    })
                    .create()
                    .show();
        }
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onTimePickerEvent(TimePickerEvent event) {
        switch (event.type) {
            case Constants.TYPE_APPOINTMENT:
                timeAppointment.setText(event.value);
                boolean enabledAppointment = prefs.getBoolean(Prefs.ENABLE_APPOINTMENT_PUSH);
                if (enabledAppointment) {
                    AlarmUtil.setNext(getApplicationContext(), Constants.TYPE_APPOINTMENT,
                            false, getClass());
                } else {
                    AlarmUtil.cancel(getApplicationContext(), Constants.TYPE_APPOINTMENT);
                }
                break;
            case Constants.TYPE_EXECUTION:
                timeExecution.setText(event.value);
                boolean enabledExecution = prefs.getBoolean(Prefs.ENABLE_EXECUTION_PUSH);
                if (enabledExecution) {
                    AlarmUtil.setNext(getApplicationContext(), Constants.TYPE_EXECUTION,
                            false, getClass());
                } else {
                    AlarmUtil.cancel(getApplicationContext(), Constants.TYPE_EXECUTION);
                }
                break;
        }
    }

    @OnCheckedChanged({R.id.box_appointment, R.id.box_execution})
    public void onCheckChanged(CompoundButton button, boolean checked) {
        switch (button.getId()) {
            case R.id.box_appointment:
                prefs.putBoolean(Prefs.ENABLE_APPOINTMENT_PUSH, checked);
                if (checked) {
                    AlarmUtil.setNext(getApplicationContext(), Constants.TYPE_APPOINTMENT,
                            false, getClass());
                } else {
                    AlarmUtil.cancel(getApplicationContext(), Constants.TYPE_APPOINTMENT);
                }
                break;
            case R.id.box_execution:
                prefs.putBoolean(Prefs.ENABLE_EXECUTION_PUSH, checked);
                if (checked) {
                    AlarmUtil.setNext(getApplicationContext(), Constants.TYPE_EXECUTION,
                            false, getClass());
                } else {
                    AlarmUtil.cancel(getApplicationContext(), Constants.TYPE_EXECUTION);
                }
                break;
        }
    }

    @OnClick(R.id.time_appointment)
    void onAppointment() {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.EXTRA_TYPE, Constants.TYPE_APPOINTMENT);
        timePickerFragment.setArguments(bundle);
        timePickerFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        timePickerFragment.show(getChildFragmentManager(), getString(R.string.app_name));
    }

    @OnClick(R.id.time_execution)
    void onExecution() {
        TimePickerFragment timePickerFragment = new TimePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.EXTRA_TYPE, Constants.TYPE_EXECUTION);
        timePickerFragment.setArguments(bundle);
        timePickerFragment.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        timePickerFragment.show(getChildFragmentManager(), getString(R.string.app_name));
    }

    @OnClick(R.id.star1)
    void onStar1() {
        setStars(1);
        getMainActivity().onStartSafelyActivity(ReviewActivity.class);
    }

    @OnClick(R.id.star2)
    void onStar2() {
        setStars(2);
        getMainActivity().onStartSafelyActivity(ReviewActivity.class);
    }

    @OnClick(R.id.star3)
    void onStar3() {
        setStars(3);
        getMainActivity().onStartSafelyActivity(ReviewActivity.class);
    }

    @OnClick(R.id.star4)
    void onStar4() {
        setStars(4);
        openApp();
    }

    @OnClick(R.id.star5)
    void onStar5() {
        setStars(5);
        openApp();
    }

    private void setStars(int count) {
        star1.setImageResource(count > 0 ? R.drawable.ic_star_18dp : R.drawable.ic_star_border_18dp);
        star2.setImageResource(count > 1 ? R.drawable.ic_star_18dp : R.drawable.ic_star_border_18dp);
        star3.setImageResource(count > 2 ? R.drawable.ic_star_18dp : R.drawable.ic_star_border_18dp);
        star4.setImageResource(count > 3 ? R.drawable.ic_star_18dp : R.drawable.ic_star_border_18dp);
        star5.setImageResource(count > 4 ? R.drawable.ic_star_18dp : R.drawable.ic_star_border_18dp);
        prefs.putInt(Prefs.STARS_COUNT, count);
    }

    @SuppressWarnings("all")
    private void openApp() {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + getActivity().getPackageName())));
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" +
                            getActivity().getPackageName())));
        }
    }
}
