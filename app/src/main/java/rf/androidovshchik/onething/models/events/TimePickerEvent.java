package rf.androidovshchik.onething.models.events;

public class TimePickerEvent {

    public int type;

    public String value;

    public TimePickerEvent(int type, String value) {
        this.type = type;
        this.value = value;
    }
}
