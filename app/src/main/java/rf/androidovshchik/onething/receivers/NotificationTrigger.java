package rf.androidovshchik.onething.receivers;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;

import rf.androidovshchik.onething.Constants;
import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.ui.main.MainActivity;
import rf.androidovshchik.onething.utils.AlarmUtil;
import timber.log.Timber;

public class NotificationTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("NotificationTrigger: received");
		switch (intent.getIntExtra(Constants.EXTRA_TYPE, Constants.TYPE_APPOINTMENT)) {
			case Constants.TYPE_APPOINTMENT:
				showNotification(context, R.string.push_appointment, 1);
				AlarmUtil.setNext(context, Constants.TYPE_APPOINTMENT, true, getClass());
				break;
			case Constants.TYPE_EXECUTION:
				showNotification(context, R.string.push_execution, 2);
				AlarmUtil.setNext(context, Constants.TYPE_EXECUTION, true, getClass());
				break;
		}
	}

	@SuppressWarnings("all")
	private void showNotification(Context context, @StringRes int text, int id) {
		NotificationManager notificationManager = (NotificationManager)
				context.getSystemService(Context.NOTIFICATION_SERVICE);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(context.getString(R.string.app_name),
					context.getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
			notificationManager.createNotificationChannel(channel);
		}
		Intent intent = new Intent(context, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.putExtra(MainActivity.EXTRA_TAB, 2);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context,
				context.getString(R.string.app_name))
				.setSmallIcon(R.drawable.ic_format_color_text)
				.setContentTitle(context.getString(R.string.settings_reminder))
				.setContentText(context.getString(text))
				.setContentIntent(PendingIntent.getActivity(context,0, intent, 0))
				.setDefaults(Notification.DEFAULT_SOUND)
				.setAutoCancel(true);
		notificationManager.notify(id, builder.build());
	}
}
