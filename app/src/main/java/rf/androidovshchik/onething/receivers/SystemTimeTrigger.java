package rf.androidovshchik.onething.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import rf.androidovshchik.onething.Constants;
import rf.androidovshchik.onething.models.events.SystemTimeEvent;
import rf.androidovshchik.onething.utils.AlarmUtil;
import rf.androidovshchik.onething.utils.EventUtil;
import timber.log.Timber;

@SuppressWarnings("all")
public class SystemTimeTrigger extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("SystemTimeTrigger: received");
		AlarmUtil.setNext(context, Constants.TYPE_APPOINTMENT, false, getClass());
		AlarmUtil.setNext(context, Constants.TYPE_EXECUTION, false, getClass());
		EventUtil.postSticky(new SystemTimeEvent());
	}
}
