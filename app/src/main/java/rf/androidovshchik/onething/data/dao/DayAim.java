package rf.androidovshchik.onething.data.dao;

import android.content.ContentValues;
import android.database.Cursor;

import org.joda.time.LocalDate;

import rf.androidovshchik.onething.data.dbhelpers.DayAimDbHelper;

public class DayAim implements Element {
    private LocalDate date;
    private String aim;
	private boolean done;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getAim() {
        return aim;
    }

    public void setAim(String aim) {
        this.aim = aim;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    @Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(DayAimDbHelper.COLUMN_YEAR, date.getYear());
		values.put(DayAimDbHelper.COLUMN_MONTH, date.getMonthOfYear());
		values.put(DayAimDbHelper.COLUMN_DAY, date.getDayOfMonth());
		values.put(DayAimDbHelper.COLUMN_AIM, aim);
		values.put(DayAimDbHelper.COLUMN_DONE, done ? 1 : 0);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
	    int year = cursor.getInt(cursor.getColumnIndexOrThrow(DayAimDbHelper.COLUMN_YEAR));
        int month = cursor.getInt(cursor.getColumnIndexOrThrow(DayAimDbHelper.COLUMN_MONTH));
        int day = cursor.getInt(cursor.getColumnIndexOrThrow(DayAimDbHelper.COLUMN_DAY));

        date = new LocalDate(year, month, day);
		aim = cursor.getString(cursor.getColumnIndexOrThrow(DayAimDbHelper.COLUMN_AIM));
		done = cursor.getInt(cursor.getColumnIndexOrThrow(DayAimDbHelper.COLUMN_DONE)) == 1;
	}

	public static DayAim of(Cursor cursor) {
        DayAim dayAim = new DayAim();
        dayAim.parseCursor(cursor);
        return dayAim;
    }
}
