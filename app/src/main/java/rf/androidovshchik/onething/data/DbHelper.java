package rf.androidovshchik.onething.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import rf.androidovshchik.onething.data.dbhelpers.DayAimDbHelper;
import rf.androidovshchik.onething.data.dbhelpers.YearAimsDbHelper;

public class DbHelper extends SQLiteOpenHelper {

    public static final int VERSION = 1;
    public static final String DATABASE_NAME = "db.sqlite";
    private static SQLiteDatabase sDb;
    private static Context sContext;

    private DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        sContext = context;
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (sDb == null) {
            sDb = new DbHelper(context).getWritableDatabase();
        }
        return sDb;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        YearAimsDbHelper yearAimsDbHelper = new YearAimsDbHelper(db);
        yearAimsDbHelper.onCreate();
        yearAimsDbHelper.insert(YearAimsDbHelper.getYearArmsSample(sContext));

        new DayAimDbHelper(db).onCreate();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}