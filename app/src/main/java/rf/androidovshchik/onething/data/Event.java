package rf.androidovshchik.onething.data;

public enum Event {
    MODIFY_YEAR_AIM,
    MODIFY_DAY_AIM,
    FINISH_YEAR_AIM,
    FINISH_YEAR_ALL_AIM,
    FINISH_DAY_AIM,
    FINISH_DAY_AIM_AFTER_DAY_FINISHED
}
