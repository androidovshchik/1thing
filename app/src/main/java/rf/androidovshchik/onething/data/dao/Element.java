package rf.androidovshchik.onething.data.dao;

import android.content.ContentValues;
import android.database.Cursor;

public interface Element {
    ContentValues toContentValues();
    void parseCursor(Cursor cursor);
}
