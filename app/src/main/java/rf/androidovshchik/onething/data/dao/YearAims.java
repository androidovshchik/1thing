package rf.androidovshchik.onething.data.dao;

import android.content.ContentValues;
import android.database.Cursor;

import rf.androidovshchik.onething.data.dbhelpers.YearAimsDbHelper;


public class YearAims implements Element {

	private int year;
    private String aim1;
    private String aim2;
    private String aim3;
    private boolean done1;
    private boolean done2;
    private boolean done3;

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getAim1() {
        return aim1;
    }

    public void setAim1(String aim1) {
        this.aim1 = aim1;
    }

    public String getAim2() {
        return aim2;
    }

    public void setAim2(String aim2) {
        this.aim2 = aim2;
    }

    public String getAim3() {
        return aim3;
    }

    public void setAim3(String aim3) {
        this.aim3 = aim3;
    }

    public boolean isDone1() {
        return done1;
    }

    public void setDone1(boolean done1) {
        this.done1 = done1;
    }

    public boolean isDone2() {
        return done2;
    }

    public void setDone2(boolean done2) {
        this.done2 = done2;
    }

    public boolean isDone3() {
        return done3;
    }

    public void setDone3(boolean done3) {
        this.done3 = done3;
    }

    @Override
	public ContentValues toContentValues() {
		ContentValues values = new ContentValues();
		values.put(YearAimsDbHelper.COLUMN_YEAR, year);
		values.put(YearAimsDbHelper.COLUMN_AIM1, aim1);
		values.put(YearAimsDbHelper.COLUMN_AIM2, aim2);
		values.put(YearAimsDbHelper.COLUMN_AIM3, aim3);
		values.put(YearAimsDbHelper.COLUMN_DONE1, done1 ? 1 : 0);
		values.put(YearAimsDbHelper.COLUMN_DONE2, done2 ? 1 : 0);
		values.put(YearAimsDbHelper.COLUMN_DONE3, done3 ? 1 : 0);
		return values;
	}

	@Override
	public void parseCursor(Cursor cursor) {
		year = cursor.getInt(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_YEAR));
		aim1 = cursor.getString(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_AIM1));
		aim2 = cursor.getString(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_AIM2));
		aim3 = cursor.getString(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_AIM3));
		done1 = cursor.getInt(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_DONE1)) == 1;
		done2 = cursor.getInt(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_DONE2)) == 1;
		done3 = cursor.getInt(cursor.getColumnIndexOrThrow(YearAimsDbHelper.COLUMN_DONE3)) == 1;
	}

    public static YearAims of(Cursor cursor) {
        YearAims yearAims = new YearAims();
        yearAims.parseCursor(cursor);
        return yearAims;
    }

}
