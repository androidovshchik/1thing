package rf.androidovshchik.onething.data.dbhelpers;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

import rf.androidovshchik.onething.data.dao.DayAim;

public class DayAimDbHelper {
    private SQLiteDatabase db;

    public static final String TABLE = "days";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MONTH = "month";
    public static final String COLUMN_DAY = "day";
    public static final String COLUMN_AIM = "aim";
    public static final String COLUMN_DONE = "done";

    public DayAimDbHelper(SQLiteDatabase db) {
        this.db = db;
    }

    public void onCreate() {
        db.execSQL("create table " + TABLE + " (" +
                COLUMN_YEAR + " INTEGER, " +
                COLUMN_MONTH + " INTEGER, " +
                COLUMN_DAY + " INTEGER ," +
                COLUMN_AIM + " TEXT, " +
                COLUMN_DONE + " BOOLEAN DEFAULT (0) NOT NULL)");
    }

    public DayAim selectOne(LocalDate date) {
        try (Cursor cursor = db.query(TABLE, null,
                COLUMN_YEAR + " = ? AND " + COLUMN_MONTH + " = ? AND " + COLUMN_DAY + " = ?",
                new String[] {
                        Integer.toString(date.getYear()),
                        Integer.toString(date.getMonthOfYear()),
                        Integer.toString(date.getDayOfMonth())
                },
                null, null, null)) {

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                return DayAim.of(cursor);
            } else {
                return null;
            }
        }
    }

    public List<DayAim> selectInYear(int year) {
        try (Cursor cursor = db.query(TABLE, null,COLUMN_YEAR + " = ?",
                new String[] { Integer.toString(year) },null, null, null)) {

            List<DayAim> elements = new ArrayList<>();
            while(cursor.moveToNext()) {
                elements.add(DayAim.of(cursor));
            }

            return elements;
        }
    }

    public List<DayAim> selectAll() {
        try (Cursor cursor = db.query(TABLE, null,null, null,null, null, null)) {

            List<DayAim> elements = new ArrayList<>();
            while(cursor.moveToNext()) {
                elements.add(DayAim.of(cursor));
            }

            return elements;
        }
    }

    public void update(DayAim dayAim) {
        ContentValues cv = dayAim.toContentValues();
        LocalDate date = dayAim.getDate();
        db.update(TABLE, cv,
                COLUMN_YEAR + " = ? AND " + COLUMN_MONTH + " = ? AND " + COLUMN_DAY + " = ?",
                new String[] {
                        Integer.toString(date.getYear()),
                        Integer.toString(date.getMonthOfYear()),
                        Integer.toString(date.getDayOfMonth())
                });
    }

    public void insert(DayAim dayAim) {
        ContentValues cv = dayAim.toContentValues();
        db.insert(TABLE, null, cv);
    }

    public void updateOrInsert(DayAim dayAim) {
        DayAim one = selectOne(dayAim.getDate());
        if (one == null) {
            insert(dayAim);
        } else {
            update(dayAim);
        }
    }
}
