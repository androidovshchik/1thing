package rf.androidovshchik.onething.data.dbhelpers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import org.joda.time.LocalDate;

import rf.androidovshchik.onething.R;
import rf.androidovshchik.onething.data.dao.YearAims;

public class YearAimsDbHelper {
    private SQLiteDatabase db;

    public static final String TABLE = "years";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_AIM1 = "aim1";
    public static final String COLUMN_AIM2 = "aim2";
    public static final String COLUMN_AIM3 = "aim3";
    public static final String COLUMN_DONE1 = "done1";
    public static final String COLUMN_DONE2 = "done2";
    public static final String COLUMN_DONE3 = "done3";

    public YearAimsDbHelper(SQLiteDatabase db) {
        this.db = db;
    }

    public void onCreate() {
        db.execSQL("create table " + TABLE + " (" +
                COLUMN_YEAR + " INTEGER primary key, " +
                COLUMN_AIM1 + " TEXT, " +
                COLUMN_AIM2 + " TEXT, " +
                COLUMN_AIM3 + " TEXT, " +
                COLUMN_DONE1 + " BOOLEAN DEFAULT (0) NOT NULL, " +
                COLUMN_DONE2 + " BOOLEAN DEFAULT (0) NOT NULL, " +
                COLUMN_DONE3 + " BOOLEAN DEFAULT (0) NOT NULL)");
    }

    public YearAims selectOne(int year) {
        try (Cursor cursor = db.query(TABLE, null, COLUMN_YEAR + " = ?",
                new String[] { Integer.toString(year) },
                null, null, null)) {

            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                return YearAims.of(cursor);
            } else {
                return null;
            }
        }
    }

    public void update(YearAims yearAims) {
        ContentValues cv = yearAims.toContentValues();
        db.update(TABLE, cv, COLUMN_YEAR + " = ?",
                new String[] { Integer.toString(yearAims.getYear()) });
    }

    public void insert(YearAims yearAims) {
        ContentValues cv = yearAims.toContentValues();
        db.insert(TABLE, null, cv);
    }

    public void updateOrInsert(YearAims yearAims) {
        YearAims one = selectOne(yearAims.getYear());
        if (one == null) {
            insert(yearAims);
        } else {
            update(yearAims);
        }
    }

    public static YearAims getYearArmsSample(Context context) {
        YearAims yearAims = new YearAims();
        yearAims.setAim1(context.getResources().getString(R.string.aim1));
        yearAims.setAim2(context.getResources().getString(R.string.aim2));
        yearAims.setAim3(context.getResources().getString(R.string.aim3));
        yearAims.setDone1(true);
        yearAims.setDone2(false);
        yearAims.setDone3(false);
        yearAims.setYear(LocalDate.now().getYear());

        return yearAims;
    }

}
