package rf.androidovshchik.onething;

public interface Constants {

    String EXTRA_TYPE = "type";
    int TYPE_APPOINTMENT = 0;
    int TYPE_EXECUTION = 1;
}
