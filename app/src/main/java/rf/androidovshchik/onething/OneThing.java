package rf.androidovshchik.onething;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;

import org.acra.ACRA;

import java.io.File;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import rf.androidovshchik.onething.data.DbHelper;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.models.events.SuggestEvent;
import rf.androidovshchik.onething.utils.EventUtil;
import timber.log.Timber;

public class OneThing extends Application {

    public static final String API_key = "656b2504-aa9e-4a35-a03c-3b408176df5e";
    public static final int SESSION_IN_SECONDS = 30;
    private Prefs prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(getApplicationContext());
        } else {
            ACRA.init(this);
        }
        prefs = new Prefs(getApplicationContext());
        if (!prefs.has(Prefs.TIME_APPOINTMENT_PUSH)) {
            prefs.putString(Prefs.TIME_APPOINTMENT_PUSH, "09:00");
        }
        if (!prefs.has(Prefs.TIME_EXECUTION_PUSH)) {
            prefs.putString(Prefs.TIME_EXECUTION_PUSH, "21:00");
        }
        if (!prefs.has(Prefs.FIRST_LAUNCH_TIME)) {
            Observable.just(true).delay(3, TimeUnit.MINUTES)
                    .subscribe((Boolean value) -> {
                        Timber.d("Finish waiting 3 minutes pause");
                        if (!prefs.has(Prefs.SUGGESTED_AFTER_3_MINUTES)) {
                            prefs.putBoolean(Prefs.SUGGESTED_AFTER_3_MINUTES, true);
                            EventUtil.postSticky(new SuggestEvent());
                        }
                    });
        }

        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder(API_key);

        if (!prefs.has(Prefs.FIRST_LAUNCH_TIME)) {
            configBuilder.handleFirstActivationAsUpdate(true);
        }
        YandexMetricaConfig extendedConfig = configBuilder.build();
        YandexMetrica.activate(getApplicationContext(), extendedConfig);
        YandexMetrica.enableActivityAutoTracking(this);
        YandexMetrica.setSessionTimeout(SESSION_IN_SECONDS);
    }
}
