package rf.androidovshchik.onething.utils;

import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.ui.main.SettingsFragment;

public class SuggestUtil {

    public static boolean needShow(Prefs prefs, Class clss) {
        if (!prefs.has(Prefs.SUGGESTED_AFTER_FIRST_SETTINGS) && clss.equals(SettingsFragment.class)) {
            return cancelSuggestions(prefs, 0);
        }
        long delay = System.currentTimeMillis() - prefs.getLong(Prefs.FIRST_LAUNCH_TIME);
        if (!prefs.has(Prefs.SUGGESTED_AFTER_3_MINUTES) && delay >= 3 * AlarmUtil.MINUTE) {
            return cancelSuggestions(prefs, 3);
        }
        if (!prefs.has(Prefs.SUGGESTED_AFTER_3_DAYS) && delay >= 3 * 24 * 60 * AlarmUtil.MINUTE) {
            return cancelSuggestions(prefs, 3 * 24 * 60);
        }
        if (!prefs.has(Prefs.SUGGESTED_AFTER_1_WEEK) && delay >= 7 * 24 * 60 * AlarmUtil.MINUTE) {
            return cancelSuggestions(prefs, 7 * 24 * 60);
        }
        if (!prefs.has(Prefs.SUGGESTED_AFTER_2_WEEKS) && delay >= 14 * 24 * 60 * AlarmUtil.MINUTE) {
            return cancelSuggestions(prefs, 14 * 24 * 60);
        }
        return false;
    }

    public static boolean cancelSuggestions(Prefs prefs, int minutes) {
        if (minutes <= 0) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_FIRST_SETTINGS, true);
        }
        if (minutes < 0 || minutes >= 3) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_3_MINUTES, true);
        }
        if (minutes < 0 || minutes >= 3 * 24 * 60) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_3_DAYS, true);
        }
        if (minutes < 0 || minutes >= 7 * 24 * 60) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_1_WEEK, true);
        }
        if (minutes < 0 || minutes >= 14 * 24 * 60) {
            prefs.putBoolean(Prefs.SUGGESTED_AFTER_2_WEEKS, true);
        }
        return true;
    }
}