package rf.androidovshchik.onething.utils;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import java.util.Calendar;

import rf.androidovshchik.onething.Constants;
import rf.androidovshchik.onething.data.Prefs;
import rf.androidovshchik.onething.receivers.NotificationTrigger;
import timber.log.Timber;

public class AlarmUtil {

    public static final long MINUTE = 60000L;

    @SuppressWarnings("all")
    public static void setNext(Context context, int type, boolean forceNextDay, Class clss) {
        long delay = 0L;
        Prefs prefs = new Prefs(context);
        switch (type) {
            case Constants.TYPE_APPOINTMENT:
                boolean enableAppointment = prefs.has(Prefs.ENABLE_APPOINTMENT_PUSH);
                if (!enableAppointment) {
                    Timber.d("No need in appointment alarm from class " + clss.getSimpleName());
                    return;
                }
                delay = getInterval(prefs.getString(Prefs.TIME_APPOINTMENT_PUSH), forceNextDay);
                break;
            case Constants.TYPE_EXECUTION:
                boolean enableExecution = prefs.has(Prefs.ENABLE_EXECUTION_PUSH);
                if (!enableExecution) {
                    Timber.d("No need in execution alarm from class " + clss.getSimpleName());
                    return;
                }
                delay = getInterval(prefs.getString(Prefs.TIME_EXECUTION_PUSH), forceNextDay);
                break;
            default:
                Timber.w("Unknown type from class " + clss.getSimpleName());
                return;
        }
        if (delay < MINUTE) {
            delay = MINUTE;
        }
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationTrigger.class);
        intent.putExtra(Constants.EXTRA_TYPE, type);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, type, intent, 0);
        alarmManager.cancel(pendingIntent);
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.ELAPSED_REALTIME_WAKEUP,
            SystemClock.elapsedRealtime() + delay, pendingIntent);
        Timber.d("New alarm with delay in " + (delay / 1000) + " seconds from class " +
                clss.getSimpleName() + " for type " + type);
    }
    
    @SuppressWarnings("all")
    private static long getInterval(String alarmTime, boolean forceNextDay) {
        Calendar calendar = Calendar.getInstance();
        String[] alarmNumbers = alarmTime.split(":");
        int nowHours = calendar.get(Calendar.HOUR_OF_DAY), nowMinutes = calendar.get(Calendar.MINUTE);
        int alarmHours = Integer.parseInt(alarmNumbers[0]), alarmMinutes = Integer.parseInt(alarmNumbers[1]);
        int nowMinutesCount = 60 * nowHours + nowMinutes, alarmMinutesCount = 60 * alarmHours + alarmMinutes;
        if (forceNextDay || nowMinutesCount > alarmMinutesCount) {
            return (24 * 60 - nowMinutesCount + alarmMinutesCount) * MINUTE;
        } else {
            return (alarmMinutesCount - nowMinutesCount) * MINUTE;
        }
    }

    @SuppressWarnings("all")
    public static void cancel(Context context, int type) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, NotificationTrigger.class);
        intent.putExtra(Constants.EXTRA_TYPE, type);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, type, intent, 0);
        alarmManager.cancel(pendingIntent);
        Timber.d("Alarm is canceled for type " + type);
    }
}
