package rf.androidovshchik.onething.utils;

import android.content.Context;
import android.graphics.Point;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class ViewUtil {

    @SuppressWarnings("all")
    public static Point getWindow(Context context) {
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }

    @SuppressWarnings("all")
    public static void showKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE))
                .toggleSoftInput(0, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    @SuppressWarnings("all")
    public static void hideKeyboard(Context context) {
        ((InputMethodManager) context.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE))
                .toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0);
    }
}